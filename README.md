# People Segmentation
In this repo I make a simple code for segmentation of people 
with the aim to create a OBS plugin for people segmentation.

## How to run
```
sudo apt install python3.8-dev

python3.8 -m venv venv
source venv/bin/activate

PYTHONPATH=venv/bin pip install -r requirements.txt

cd models 
wget https://github.com/ayoolaolafenwa/PixelLib/releases/download/0.2.0/pointrend_resnet50.pkl
cd ..

python src/main.py
python src/opencv_video_camera.py
```

References
* torch and torch vision
* deeplabv3+
* [pycocotools](https://github.com/ppwwyyxx/cocoapi)
* [pixellib](https://github.com/ayoolaolafenwa/PixelLib/blob/master/Tutorials/Pytorch_image_instance_segmentation.md)