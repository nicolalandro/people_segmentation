import pixellib
from pixellib.torchbackend.instance import instanceSegmentation

ins = instanceSegmentation()
ins.load_model("models/pointrend_resnet50.pkl")
results, output = ins.segmentImage("imgs/test.png", show_bboxes=True, output_image_name="imgs/output_image.jpg")
