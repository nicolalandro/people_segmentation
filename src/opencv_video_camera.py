# import the opencv library
import cv2

# define a video capture object
from pixellib.torchbackend.instance import instanceSegmentation

vid = cv2.VideoCapture(0)
ins = instanceSegmentation()
ins.load_model("models/pointrend_resnet50.pkl")

ins.process_camera(vid, show_frames=True, frame_name='frame')

# while (True):
#
#     # Capture the video frame
#     # by frame
#     ret, frame = vid.read()
#
#     results, output = ins.segmentFrame(frame, show_bboxes=False, extract_segmented_objects=True)
#
#     # Display the resulting frame
#     cv2.imshow('frame', output)
#
#     # the 'q' button is set as the
#     # quitting button you may use any
#     # desired button of your choice
#     if cv2.waitKey(1) & 0xFF == ord('q'):
#         break

# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()
